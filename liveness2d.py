# import the necessary packages
from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
from imutils.video import FileVideoStream
import time

from utils.utils import ear_distance,left_ear__distance,right_ear_distance,brow_eye_distance,smile_distance
import matplotlib.pyplot as plt
from gen_dict import read_dict

EYE_AR_THRESH = 0.3
EYE_AR_CONSEC_FRAMES = 1

(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

def visualize_landmark(image, shape, distance):
    leftEye = shape[lStart:lEnd]
    rightEye = shape[rStart:rEnd]

    leftEyeHull = cv2.convexHull(leftEye)
    rightEyeHull = cv2.convexHull(rightEye)

    cv2.drawContours(image, [leftEyeHull], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [rightEyeHull], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[17:21])], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[22:26])], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[60:67])], -1, (0, 255, 0), 1)



    cv2.putText(image, "SCORE: {:.2f}".format(distance), (300, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

    cv2.imshow("Frame", image)
    cv2.waitKey()
    key = cv2.waitKey(2) & 0xFF
    return key


def caculate_distance(image, detector, predictor, function_distance, visualize = False):
    image = imutils.resize(image, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale frame
    start_time = time.time()
    rects = detector(gray, 0)
    print("time detect: " + str(time.time() - start_time))
    #if there are more than 1 person in image
    if len(rects) > 1:
        return -1
    else:
        rect = rects[0]
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        distance = function_distance(shape)

        if visualize:
            key = visualize_landmark(image, shape, distance)
            return key, distance
        return distance


def caculate_distance_from_video(video,detector, predictor, function_distance, visualize = False):
    list_distance = []
    if video is not None :
        vs = FileVideoStream(video).start()
        fileStream = True
        time.sleep(2)
        patient = 0
        while True:
            if fileStream and not vs.more():
                break
            frame = vs.read()
            if frame is None:
                continue
            try:
                key, distance = caculate_distance(frame, detector, predictor, function_distance,visualize)
                if key == ord("q"):
                    break
                list_distance.append(distance)
                patient = 0
            except Exception:
                patient = patient + 1
                if patient > 1000:
                    break
                else:
                    continue

    if visualize:
        plt.plot(np.arange(len(list_distance)), list_distance)
        plt.show()
    return list_distance

def caculate_distance_from_dict(list_dict, detector, predictor, function_distance, visualize = False):
    list_distance = []
    for dict in list_dict:
        image = dict["image"]
        distance = caculate_distance(image, detector, predictor, function_distance,visualize)
        list_distance.append(distance)
    if visualize:
        plt.plot(np.arange(len(list_distance)), list_distance)
        plt.show()
    return list_distance


def detect_close_left_eye(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_close_left_eye")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, left_ear__distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, left_ear__distance, visualize)

    # if np.mean(list_distance) < 0.15:
    if np.percentile(list_distance, 25) < 0.15:
        return True
    else:
        return False

def detect_close_right_eye(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_close_right_eye")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, right_ear_distance,visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, right_ear_distance,visualize)

    if np.percentile(list_distance, 25) < 0.15:
        return True
    else:
        return False

def detect_blink(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_blink")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, ear_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, ear_distance, visualize)

    if np.var(list_distance) > 0.15:
        return True
    else:
        return False
def detect_knit_brows(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_knit_brows")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, brow_eye_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, brow_eye_distance, visualize)

    if np.mean(list_distance) > 75:
        return True
    else:
        return False

def detect_smile(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_smile")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, smile_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, smile_distance, visualize)


    if np.var(list_distance) > 6:
        return True
    else:
        return False

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--shape-predictor", default='model/shape_predictor_68_face_landmarks.dat',
                    help="path to facial landmark predictor")
    ap.add_argument("-v", "--video", type=str, default="data/IMG_1862.MOV",
                    help="path to input video file")
    ap.add_argument("-d", "--dictionary", type=str, default="data/nham_mat_phai.pkl")
    args = vars(ap.parse_args())


    # print("[INFO] loading facial landmark predictor...")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(args["shape_predictor"])
    #
    # list_dict = read_dict(args["dictionary"])
    # result = detect_close_left_eye(list_dict, detector, predictor,video=args["video"],visualize=True)
    # print(result)
    import os
    list_dist = []
    for path in os.listdir('data/liveness-2019-10-22_03h51m58s_5341/CLOSE_RIGHT_EYE/request_0'):
        image = cv2.imread('data/liveness-2019-10-22_03h51m58s_5341/CLOSE_RIGHT_EYE/request_0/'+ path)
        # cv2.imshow('img',image)
        # cv2.waitKey()
        start_time = time.time()
        dist = caculate_distance(image, detector, predictor, right_ear_distance, visualize=False)
        print(str(time.time() - start_time))
        list_dist.append(dist)
    print(np.percentile(list_dist, 25))
    # image = cv2.imread('data/72741850_430102517647967_5130400643378315264_n.jpg')
    # caculate_distance(image, detector, predictor, right_ear_distance, visualize=True)