import argparse
import time

from imutils.video import FileVideoStream
from datetime import datetime
import pickle


ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", type=str, default="data/nham_mat_phai.mp4",
	help="path to input video file")
args = vars(ap.parse_args())


def create_list_dict(video_path):
    vs = FileVideoStream(video_path).start()
    fileStream = True
    time.sleep(1)

    list_dict = []
    while True:
        if fileStream and not vs.more():
            break
        frame = vs.read()
        if frame is None:
            continue
        now = datetime.now()
        list_dict.append({"image": frame, "timestamp": datetime.timestamp(now)})
    return list_dict

def read_dict(save_path):
    file = open(save_path, 'rb')
    data = pickle.load(file)
    file.close()
    return data

def creat_and_save_dict(video_path, save_path):
    list_dict = create_list_dict(video_path)

    file = open(save_path, 'wb')
    pickle.dump(list_dict, file)
    file.close()

if __name__ == "__main__":
    creat_and_save_dict('D:\\blink-detection\IMG_1875.MOV', 'data/nham_mat_phai.pkl')
    # dict = create_list_dict("data/nham_mat_phai.mp4")
    # dict1 = read_dict('data/nham_mat_phai.pkl')
