import argparse
import utils
from utils.ddfa import ToTensorGjz, NormalizeGjz, str2bool
import torch
import mobilenet_v1
import torch.backends.cudnn as cudnn
import dlib
import scipy.io as sio
import torchvision.transforms as transforms
import numpy as np
from utils.inference import get_suffix, parse_roi_box_from_landmark, crop_img, predict_68pts, dump_to_ply, dump_vertex, \
    draw_landmarks, predict_dense, parse_roi_box_from_bbox, get_colors, write_obj_with_colors
from utils.estimate_pose import parse_pose
import cv2
import time

from imutils import face_utils
import imutils
from imutils.video import FileVideoStream
import time
from utils.utils import ear_distance,left_ear__distance,right_ear_distance,brow_eye_distance,smile_distance, node_pixel
import matplotlib.pyplot as plt
from gen_dict import read_dict
from detect_dlib import load_model, left_eye_status, right_eye_status

STD_SIZE =120
# parser = argparse.ArgumentParser(description='3DDFA inference pipeline')
# parser.add_argument('-f', '--files', default="data/Capture.PNG", nargs='+',
#                     help='image files paths fed into network, single or multiple images')
# parser.add_argument('-m', '--mode', default='cpu', type=str, help='gpu or cpu mode')
# args = parser.parse_args()


def load_pretrain_model():
    checkpoint_fp = 'models/phase1_wpdc_vdc.pth.tar'
    arch = 'mobilenet_1'

    checkpoint = torch.load(checkpoint_fp, map_location=lambda storage, loc: storage)['state_dict']
    model = getattr(mobilenet_v1, arch)(num_classes=62)  # 62 = 12(pose) + 40(shape) +10(expression)

    model_dict = model.state_dict()
    # because the model is trained by multiple gpus, prefix module should be removed
    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]
    model.load_state_dict(model_dict)
    model.eval()
    return model

def load_dlib_model():
    dlib_landmark_model = 'model/shape_predictor_68_face_landmarks.dat'
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(dlib_landmark_model)

    # face_regressor = dlib.shape_predictor(dlib_landmark_model)
    # face_detector = dlib.get_frontal_face_detector()

    # return face_regressor, face_detector
    return predictor, detector

def detect_pose(img_ori):
    gray = cv2.cvtColor(img_ori, cv2.COLOR_BGR2GRAY)

    rects = face_detector(gray, 0)

    for rect in rects:
        pts = face_regressor(img_ori, rect).parts()
        pts = np.array([[pt.x, pt.y] for pt in pts]).T
        roi_box = parse_roi_box_from_landmark(pts)
        img = crop_img(img_ori, roi_box)
        img = cv2.resize(img, dsize=(STD_SIZE, STD_SIZE), interpolation=cv2.INTER_LINEAR)

        input = transform(img).unsqueeze(0)
        with torch.no_grad():
            param = model(input)
            param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

        pts68 = predict_68pts(param, roi_box)
        P, pose, pose_angle = parse_pose(param)
        return pose_angle


EYE_AR_THRESH = 0.3
EYE_AR_CONSEC_FRAMES = 1

(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

def caculate_distance(image, detector, predictor, function_distance, visualize = False):
    image = imutils.resize(image, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale frame
    # start_time = time.time()
    rects = detector(gray, 0)
    # print("time detect: " + str(time.time() - start_time))
    #if there are more than 1 person in image
    if len(rects) > 1:
        return -1, -1
    else:
        rect = rects[0]
        shape = predictor(gray, rect)
        if function_distance =='pose':
            pts = shape.parts()
            pts = np.array([[pt.x, pt.y] for pt in pts]).T
            roi_box = parse_roi_box_from_landmark(pts)
            img = crop_img(image, roi_box)
            img = cv2.resize(img, dsize=(STD_SIZE, STD_SIZE), interpolation=cv2.INTER_LINEAR)

            input = transform(img).unsqueeze(0)
            with torch.no_grad():
                param = model(input)
                param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

            P, pose, pose_angle = parse_pose(param)
            distance =  pose_angle[0]
            shape = face_utils.shape_to_np(shape)
        else:
            shape = face_utils.shape_to_np(shape)
            distance = function_distance(shape)

        if visualize:
            key = visualize_landmark(image, shape, distance)
            return key, distance
        return -1, distance
# def caculate_distance(image, detector, predictor, function_distance, visualize = False):
#     # print("caculate distance")
#     image = imutils.resize(image, width=450)
#     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#
#     start_time = time.time()
#     rects = detector(gray, 0)
#     print("time detect: "+ str(time.time() - start_time))
#     start_time = time.time()
#     for rect in rects:
#         pts = predictor(image, rect).parts()
#         # print("time predict: " + str(time.time() - start_time))
#         start_time = time.time()
#         pts = np.array([[pt.x, pt.y] for pt in pts]).T
#         roi_box = parse_roi_box_from_landmark(pts)
#         img = crop_img(image, roi_box)
#         img = cv2.resize(img, dsize=(STD_SIZE, STD_SIZE), interpolation=cv2.INTER_LINEAR)
#
#         input = transform(img).unsqueeze(0)
#         with torch.no_grad():
#             param = model(input)
#             param = param.squeeze().cpu().numpy().flatten().astype(np.float32)
#
#         pts68 = predict_68pts(param, roi_box)
#         # print("time transform: " + str(time.time() - start_time))
#         start_time = time.time()
#         shape = []
#         for i in range(68):
#             shape.append([pts68[0][i],pts68[1][i]])
#         shape = np.array(shape)
#         shape = np.int32(shape)
#         distance = function_distance(shape)
#
#         if visualize:
#             key = visualize_landmark(image, shape, distance)
#             return key, distance
#         return -1, distance


def caculate_distance_from_video(video,detector, predictor, function_distance, visualize = False):
    print("caculate distance")
    list_distance = []
    if video is not None :
        vs = FileVideoStream(video).start()
        fileStream = True
        time.sleep(2)
        patient = 0
        while True:
            if fileStream and not vs.more():
                break
            frame = vs.read()
            if frame is None:
                continue
            # try:
            # time_start = time.time()
            key, distance = caculate_distance(frame, detector, predictor, function_distance,visualize)
            # print(str(time.time() - time_start))
            if key == ord("q"):
                cv2.destroyAllWindows()
                break
            list_distance.append(distance)
            patient = 0
            # except Exception as e:
            #     print(e)
            #     patient = patient + 1
            #     if patient > 1000:
            #         break
            #     else:
            #         continue

    if visualize:
        plt.plot(np.arange(len(list_distance)), list_distance)
        plt.show()
    return list_distance

def caculate_distance_from_dict(list_dict, detector, predictor, function_distance, visualize = False):
    list_distance = []
    for dict in list_dict:
        image = dict["image"]
        key, distance = caculate_distance(image, detector, predictor, function_distance,visualize)
        if key == ord("q"):
            cv2.destroyAllWindows()
            break
        list_distance.append(distance)
    if visualize:
        plt.plot(np.arange(len(list_distance)), list_distance)
        plt.show()
    return list_distance

def detect_blink(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_blink")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, ear_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, ear_distance, visualize)

    if np.var(list_distance) > 0.15:
        return True
    else:
        return False
def detect_knit_brows(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_knit_brows")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, brow_eye_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, brow_eye_distance, visualize)

    if np.mean(list_distance) > 75:
        return True
    else:
        return False
def visualize_landmark(image, shape, distance):
    leftEye = shape[lStart:lEnd]
    rightEye = shape[rStart:rEnd]

    leftEyeHull = cv2.convexHull(leftEye)
    rightEyeHull = cv2.convexHull(rightEye)

    cv2.drawContours(image, [leftEyeHull], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [rightEyeHull], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[17:21])], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[22:26])], -1, (0, 255, 0), 1)
    cv2.drawContours(image, [cv2.convexHull(shape[60:67])], -1, (0, 255, 0), 1)



    cv2.putText(image, "SCORE: {:.2f}".format(distance), (300, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

    cv2.imshow("Frame", image)
    key = cv2.waitKey(2) & 0xFF
    return key

def detect_smile(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_smile")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, smile_distance, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, smile_distance, visualize)

    if np.mean(list_distance) > 6:
        return True
    else:
        return False

def detect_left_pose_head(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_left_pose_head")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, 'pose', visualize)
    else:
        image = list_dict.values()[-1]
        pose = detect_pose(image)
        if pose[0] > 15:
            return True
        else:
            return False

    if np.percentile(list_distance, 25) > 15:
        return True
    else:
        return False
def detect_right_pose_head(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_left_pose_head")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, 'pose', visualize)
    else:
        image = list_dict.values()[-1]
        pose = detect_pose(image)
        if pose[0] <- 15:
            return True
        else:
            return False

    if np.percentile(list_distance, 25) < -15:
        return True
    else:
        return False

def detect_nod(list_dict, detector, predictor, video = None, visualize = False):
    print("detect_left_pose_head")
    if video is not None:
        list_distance = caculate_distance_from_video(video, detector, predictor, node_pixel, visualize)
    else:
        list_distance = caculate_distance_from_dict(list_dict, detector, predictor, node_pixel, visualize)

    if np.var(list_distance) > 0.15:
        return True
    else:
        return False

def detect_eye(list_dict, classify_model, detector, predictor,function_status,video = None, visualize = False):
    num_frame = 0
    close_frame = 0
    if video is not None:
        video_capture = FileVideoStream(video)
        video_capture = video_capture.start()
        while True:
            if not video_capture.more():
                break
            frame = video_capture.read()
            if frame is not None:
                eye_status, frame = function_status(classify_model, frame, detector, predictor)
                if eye_status == 0:
                    close_frame += 1
                num_frame += 1
                if visualize:
                    cv2.imshow("Face Liveness Detector", frame)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
        cv2.destroyAllWindows()
    else:
        for dict in list_dict:
            frame = dict["image"]
            eye_status, frame = function_status(classify_model, frame, detector, predictor)
            if eye_status == 0:
                close_frame += 1
            num_frame += 1
            if visualize:
                cv2.imshow("Face Liveness Detector", frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
    return close_frame, num_frame

def detect_close_left_eye(list_dict, classify_model, detector, predictor, video = None, visualize = False):
    print("detect_close_left_eye")
    close_frame, num_frame = detect_eye(list_dict, classify_model, detector, predictor,left_eye_status, video, visualize)
    print(close_frame/num_frame)
    if close_frame/num_frame > 0.7:
        return True
    else:
        return False
def detect_close_right_eye(list_dict, classify_model, detector, predictor, video = None, visualize = False):
    print("detect_close_right_eye")
    close_frame, num_frame = detect_eye(list_dict, classify_model, detector, predictor,right_eye_status, video, visualize)
    print(close_frame/num_frame)
    if close_frame/num_frame > 0.7:
        return True
    else:
        return False



transform = transforms.Compose([ToTensorGjz(), NormalizeGjz(mean=127.5, std=128)])
model = load_pretrain_model()

if __name__ == '__main__':
    # ===================load model ================="
    print("model is loading")

    face_regressor, face_detector = load_dlib_model()
    # tri = sio.loadmat('visualize/tri.mat')['tri']
    print("model is loaded")


    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--shape-predictor", default='model/shape_predictor_68_face_landmarks.dat',
                    help="path to facial landmark predictor")
    ap.add_argument("-v", "--video", type=str, default="data/IMG_1876.MOV",
                    help="path to input video file")
    ap.add_argument("-d", "--dictionary", type=str, default="data/nham_mat_phai.pkl")
    args = vars(ap.parse_args())

    classify_model = load_model()

    result = detect_close_right_eye("",classify_model, face_detector, face_regressor,video=args["video"],visualize=False)
    print(result)
    # print(str(time.time()-time_s))
    # image = cv2.imread('data/nghieng1.jpg')
    # pose  = detect_pose(image)
    # print(pose)


