import argparse
import dlib

import os
import cv2
import face_recognition
import numpy as np
from tqdm import tqdm
from collections import defaultdict
from imutils.video import FileVideoStream
from eye_status import *
from imutils import face_utils
import imutils


def load_model():
	json_file = open('models/classify_model/model.json', 'r')
	loaded_model_json = json_file.read()
	json_file.close()
	loaded_model = model_from_json(loaded_model_json)
	# load weights into new model
	loaded_model.load_weights("models/classify_model/model.h5")
	loaded_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
	return loaded_model

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((shape.num_parts, 2), dtype=dtype)

	# loop over all facial landmarks and convert them
	# to a 2-tuple of (x, y)-coordinates
	for i in range(0, shape.num_parts):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	# return the list of (x, y)-coordinates
	return coords
def left_eye_status(model, frame, detector, predictor):
    eye_status = 1
    image = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 0)

    if len(np.array(rects)) > 1 or len(np.array(rects))==0:
        return eye_status,image
    else:
        rect = rects[0]
        shape = predictor(gray, rect)
        shape = shape_to_np(shape)

        l_center = (shape[36]+shape[39])//2
        l_w = shape[39][0] - shape[36][0]
        l_y1, l_y2, l_x1, l_x2, = l_center[1] - int(0.5*l_w) ,l_center[1] + int(0.5*l_w), l_center[0] - int(l_w) ,l_center[0] + int(0.75*l_w)
        l_y1 = np.clip(l_y1, 0, 450)
        l_y2 = np.clip(l_y2, 0, 450)
        l_x1 = np.clip(l_x1, 0, 450)
        l_x2 = np.clip(l_x2, 0, 450)

        lefteye = image[l_y1 : l_y2, l_x1: l_x2,:]
        color = (0, 255, 0)
        if lefteye.shape[0] > 0 and  lefteye.shape[1] >0:
            pred = predict(lefteye, model)
            if pred == 'closed':
                eye_status = 0
                color = (0, 0, 255)
        cv2.rectangle(image, (l_x1, l_y1), (l_x2, l_y2), color, 2)
    return eye_status, image

def right_eye_status(model, frame, detector, predictor):
    eye_status = 1
    image = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 0)

    if len(np.array(rects)) > 1 or len(np.array(rects))==0:
        return eye_status,image
    else:
        rect = rects[0]
        shape = predictor(gray, rect)
        shape = shape_to_np(shape)

        r_x1, r_x2, r_y1, r_y2  = shape[44][1], shape[47][1], shape[42][0], shape[45][0]
        r_h = r_x2 - r_x1
        r_w = r_y2 - r_y1

        r_center = (shape[45]+shape[42])//2
        r_w = shape[45][0] - shape[42][0]
        r_y1, r_y2, r_x1, r_x2, = r_center[1] - int(0.5*r_w) ,r_center[1] + int(0.5*r_w), r_center[0] - int(0.75*r_w) ,r_center[0] + int(r_w)
        r_y1 = np.clip(r_y1, 0, 450)
        r_y2 = np.clip(r_y2, 0, 450)
        r_x1 = np.clip(r_x1, 0, 450)
        r_x2 = np.clip(r_x2, 0, 450)
        righteye = image[r_y1: r_y2, r_x1: r_x2,:]

        color = (0, 255, 0)
        if righteye.shape[0] > 0 and  righteye.shape[1] > 0:
            pred = predict(righteye, model)
            if pred == 'closed':
                eye_status = 0
                color = (0, 0, 255)
        cv2.rectangle(image, (r_x1, r_y1), (r_x2, r_y2), color, 2)
    return eye_status, image


def detect_and_display(model, frame, detector, predictor):
    # image = cv2.resize(frame, (450, 450))
    image = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 0)

    if len(np.array(rects)) > 1 or len(np.array(rects))==0:
        return image
    else:
        rect = rects[0]
        shape = predictor(gray, rect)
        shape = shape_to_np(shape)

        (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
        (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]
        color = (0, 0, 255)

        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)

        # cv2.drawContours(image, [leftEyeHull], -1, (0, 255, 0), 1)
        # cv2.drawContours(image, [rightEyeHull], -1, (0, 255, 0), 1)
        # cv2.circle(image, (int(shape[42][0]), int(shape[42][1])), 10, (255, 255, 255), -11)

        l_center = (shape[36]+shape[39])//2
        l_w = shape[39][0] - shape[36][0]
        l_y1, l_y2, l_x1, l_x2, = l_center[1] - int(0.5*l_w) ,l_center[1] + int(0.5*l_w), l_center[0] - int(l_w) ,l_center[0] + int(0.75*l_w)
        l_y1 = np.clip(l_y1, 0, 450)
        l_y2 = np.clip(l_y2, 0, 450)
        l_x1 = np.clip(l_x1, 0, 450)
        l_x2 = np.clip(l_x2, 0, 450)

        lefteye = image[l_y1 : l_y2, l_x1: l_x2,:]
        color = (0, 255, 0)
        if lefteye.shape[0] > 0 and  lefteye.shape[1] >0:
            pred = predict(lefteye, model)
            if pred == 'closed':
                eye_status = '0'
                color = (0, 0, 255)
        cv2.rectangle(image, (l_x1, l_y1), (l_x2, l_y2), color, 2)

        # l_x1, l_x2, l_y1, l_y2  = shape[37][1], shape[41][1], shape[36][0], shape[39][0]
        # l_h = l_x2 - l_x1
        # lefteye = image[l_x1 - int(2*l_h) :l_x2 + int(2*l_h), l_y1 - int(0.5*l_w):l_y2 + int(0.5*l_w),:]


        r_x1, r_x2, r_y1, r_y2  = shape[44][1], shape[47][1], shape[42][0], shape[45][0]
        r_h = r_x2 - r_x1
        r_w = r_y2 - r_y1

        r_center = (shape[45]+shape[42])//2
        r_w = shape[45][0] - shape[42][0]
        r_y1, r_y2, r_x1, r_x2, = r_center[1] - int(0.5*r_w) ,r_center[1] + int(0.5*r_w), r_center[0] - int(0.75*r_w) ,r_center[0] + int(r_w)
        r_y1 = np.clip(r_y1, 0, 450)
        r_y2 = np.clip(r_y2, 0, 450)
        r_x1 = np.clip(r_x1, 0, 450)
        r_x2 = np.clip(r_x2, 0, 450)
        righteye = image[r_y1: r_y2, r_x1: r_x2,:]

        color = (0, 255, 0)
        if righteye.shape[0] > 0 and  righteye.shape[1] > 0:
            pred = predict(righteye, model)
            if pred == 'closed':
                eye_status = '0'
                color = (0, 0, 255)
        cv2.rectangle(image, (r_x1, r_y1), (r_x2, r_y2), color, 2)
        # cv2.imshow('EYE', lefteye)
        # cv2.waitKey(0)
        # cv2.rectangle(image, (shape[42][0], shape[44][1]), (shape[45][0], shape[47][1]), color, 2)
        # cv2.rectangle(image, (shape[42][0], shape[44][1]), (shape[45][0], shape[47][1]), color, 2)
    return image

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--shape-predictor", default='model/shape_predictor_68_face_landmarks.dat',
                    help="path to facial landmark predictor")
    ap.add_argument("-v", "--video", type=str, default="data/IMG_1862.MOV",
                    help="path to input video file")
    ap.add_argument("-d", "--dictionary", type=str, default="data/nham_mat_phai.pkl")
    ap.add_argument("-i", "--image", type=str, default="data/gai_demo_2.png")
    args = vars(ap.parse_args())

    model = load_model()

    print("[INFO] loading facial landmark predictor...")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(args["shape_predictor"])

    video_capture = FileVideoStream("data/IMG_1862.MOV")
    video_capture = video_capture.start()

    while True:
        if not video_capture.more():
            break

        frame = video_capture.read()
        if frame is not None:
            frame = detect_and_display(model, frame, detector, predictor)
            cv2.imshow("Face Liveness Detector", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()

    # for image_path in os.listdir('data/2019-10-19_03h07m25s_4f9c/CLOSE_RIGHT_EYE/request_0'):
    #     frame = cv2.imread(os.path.join('data/2019-10-19_03h07m25s_4f9c/CLOSE_RIGHT_EYE/request_0', image_path))
    #     frame = detect_and_display(model, frame, detector, predictor)
    #     cv2.imshow("", frame)
    #     cv2.waitKey(0)
    #     if cv2.waitKey(1) & 0xFF == ord('q'):
    #         break
    # cv2.destroyAllWindows()