import time

from liveness3d import detect_close_left_eye, detect_smile, detect_knit_brows, detect_close_right_eye, detect_blink, detect_left_pose_head, detect_right_pose_head, detect_nod
import dlib
from liveness3d import detect_pose
from gen_dict import read_dict
import argparse
import cv2
import os
from detect_dlib import load_model


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--shape-predictor", default='model/shape_predictor_68_face_landmarks.dat',
                    help="path to facial landmark predictor")
    ap.add_argument("-v", "--video", type=str, default="data/IMG_1862.MOV",
                    help="path to input video file")
    ap.add_argument("-d", "--dictionary", type=str, default="data/nham_mat_phai.pkl")
    ap.add_argument("-i", "--image", type=str, default="data/gai_demo_2.png")
    args = vars(ap.parse_args())


    print("[INFO] loading facial landmark predictor...")
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(args["shape_predictor"])

    classify_model = load_model()

    # detect some actions
    list_dict = read_dict(args["dictionary"])
    result = detect_close_left_eye(list_dict,classify_model, detector, predictor,video=args['video'],visualize=True)
    print(result)

    #detect pose
    # image = cv2.imread(args["image"])
    # start = time.time()
    # for image_path in os.listdir('./data/image'):
    #     image = cv2.imread('./data/image/'+image_path)
    #     # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #     pose = detect_pose(image)

    # print(str(time.time() - start))



